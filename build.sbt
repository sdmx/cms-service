name := """cms"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

resolvers += Resolver.sonatypeRepo("snapshots")

scalaVersion := "2.12.6"

crossScalaVersions := Seq("2.11.12", "2.12.6")

libraryDependencies += guice
libraryDependencies ++= Seq(
  "mysql" % "mysql-connector-java" % "8.0.15",
  "com.typesafe.play" %% "play-slick" % "3.0.3",
  "com.typesafe.play" %% "play-slick-evolutions" % "3.0.3",
  "com.typesafe.play" %% "play-json-joda" % "2.6.0",
  "com.typesafe.play" %% "play-mailer" % "6.0.1",
  "com.typesafe.play" %% "play-mailer-guice" % "6.0.1",

  "com.github.nscala-time" %% "nscala-time" % "2.20.0",
  "com.github.pathikrit"  %% "better-files-akka"  % "3.6.0",
  "io.lemonlabs" %% "scala-uri" % "1.2.0",
  "com.softwaremill.sttp" %% "core" % "1.3.3",

  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test,
)
