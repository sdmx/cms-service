FROM java:8-alpine

RUN apk update && apk add bash

COPY . /app/soenda/cms-service
WORKDIR /app/soenda/cms-service

COPY conf/application.docker.conf /app/soenda/cms-service/conf/application.conf

# RUN sh -c "./sbt build"

EXPOSE 9001
ENTRYPOINT ["sh", "-c", "./sbt run 9001"]
