# --- !Ups

create table `approval_flow` (
  `id` bigint unsigned auto_increment not null primary key,
  `uid` varchar(100) not null,
  `content_type` char(1) not null,
  `org_id` int unsigned not null,
  `pos_id` int unsigned not null,
  `created` timestamp not null default now(),
  `updated` timestamp not null default now(),
  foreign key (`org_id`) references `organisation`(id) on update cascade on delete cascade,
  foreign key (`pos_id`) references `position`(id) on update cascade on delete cascade
);

create table `approval_flow_detail` (
  `id` bigint unsigned auto_increment not null primary key,
  `approval_flow_id` bigint unsigned not null,
  `org_id` int unsigned not null,
  `pos_id` int unsigned not null,
  `order_num` int unsigned not null,
  `is_mandatory` boolean not null default true,
  foreign key (`approval_flow_id`) references `approval_flow`(id) on update cascade on delete cascade,
  foreign key (`org_id`) references `organisation`(id) on update cascade on delete cascade,
  foreign key (`pos_id`) references `position`(id) on update cascade on delete cascade
);

create table `approval` (
  `id` bigint unsigned auto_increment not null primary key,
  `uid` varchar(100) not null,
  `content_type` char(1) not null,
  `content_id` bigint unsigned not null, -- foreign key references `content`(id) on update cascade on delete cascade, -- included user reg & others
  `status` char(1) not null default '0',
  `created` timestamp not null default now()
);

create table `approval_manifest` (
  `id` bigint unsigned auto_increment not null primary key,
  `approval_id` bigint unsigned not null,
  `uid` varchar(100),
  `status` char(1) not null default '0',
  `is_edited` boolean not null default false,
  `is_mandatory` boolean not null default false,
  `notes` text,
  `order_num` int unsigned not null,
  `created` timestamp not null default now(),
  `updated` timestamp,
  foreign key (`approval_id`) references `approval`(id) on update cascade on delete cascade
);

# --- !Downs

drop table if exists `approval_flow_detail`;
drop table if exists `approval_flow`;
drop table if exists `approval_manifest`;
drop table if exists `approval`;
