# --- !Ups

create table `organisation` (
  `id` int unsigned auto_increment primary key,
  `parent_id` int unsigned,
  `name` varchar(255) not null,
  `type` char(1) not null
);

-- division
insert into `organisation` (`id`, `name`, `type`) values
  (1, 'Moneter', '1'),
  (2, 'Stabilitas Sistem Keuangan', '1'),
  (3, 'Sistem Pembayaran & Pengelolaan Uang Rupiah', '1'),
  (4, 'Manajemen Intern', '1');

ALTER TABLE `organisation` AUTO_INCREMENT = 5;

-- department
insert into `organisation` (`parent_id`, `name`, `type`) values
  (1, 'Departemen Kebijakan Ekonomi dan Moneter', '2'),
  (1, 'Departemen Statistik', '2'),
  (1, 'Departemen Internasional', '2'),
  (1, 'Departemen Pengelolaan dan Kepatuhan Laporan', '2'),
  (1, 'Departemen Riset Kebanksentralan', '2'),
  (1, 'Departemen Pengelolaan Moneter', '2'),
  (1, 'Departemen Pengelolaan Devisa', '2'),
  (1, 'Departemen Pengembangan Pasar Keuangan', '2'),

  (2, 'Departemen Ekonomi & Keuangan Syariah', '2'),
  (2, 'Departemen Kebijakan Makroprudensial', '2'),
  (2, 'Departemen Surveillance Sistem Keuangan', '2'),
  (2, 'Departemen Pengembangan UMKM', '2'),

  (3, 'Departemen Kebijakan & Pengawasan Sistem Pembayaran', '2'),
  (3, 'Departemen Pengelolaan Uang', '2'),
  (3, 'Departemen Penyelenggaraan Sistem Pembayaran', '2'),
  (3, 'Departemen Operasional Treasure & Pinjaman', '2'),

  (4, 'Departemen Manajemen Strategis dan Tata Kelola', '2'),
  (4, 'Departemen Manajemen Resiko', '2'),
  (4, 'Departemen Hukum', '2'),
  (4, 'Pusat Program Transformasi BI', '2'),
  (4, 'Departemen Pengelolaan Sistem Informasi', '2'),
  (4, 'Departemen Sumber Daya Manusia', '2'),
  (4, 'Departemen Keuangan', '2'),
  (4, 'Departemen Audit Intern', '2'),
  (4, 'Bank Indonesia Institute', '2'),
  (4, 'Departemen Komunikasi', '2'),
  (4, 'Departemen Pengadilan Strategis', '2'),
  (4, 'Departemen Pengelolaan Logistik dan Fasilitas', '2');

create table `position` (
  `id` int unsigned auto_increment primary key,
  `parent_id` int unsigned,
  `name` varchar(255) not null,
  `has_organisation` boolean default true
);

insert into `position` (`id`, `parent_id`, `name`, `has_organisation`) values
  (1, null, 'Gubernur', false),
  (2, 1, 'Deputi Gubernur Senior', false),
  (3, 2, 'Deputi Gubernur', false),
  (4, 3, 'Asisten Gubernur', false),
  (5, null, 'Direktur Eksekutif', true),
  (6, 5, 'Direktur', true),
  (7, 6, 'Deputi Direktur', true),
  (8, 7, 'Asisten Direktur', true),
  (9, 8, 'Manajer', true),
  (10, 9, 'Asisten Manajer', true),
  (11, 10, 'Staf', true),
  (12, 11, 'Asisten Pelaksana', true);

ALTER TABLE `position` AUTO_INCREMENT = 13;

# --- !Downs

drop table if exists `position`;
drop table if exists `organisation`;
