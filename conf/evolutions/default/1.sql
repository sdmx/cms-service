# --- !Ups

create table `category` (
  `id` int unsigned auto_increment primary key,
  `name` varchar(255) not null,
  `parent_id` int unsigned references category(id) on update cascade on delete cascade
);

create table `content` (
  `id` bigint unsigned auto_increment not null primary key,
  `content_type` char(1) not null,
  `name` varchar(255) not null,
  `body` text not null,
  `links` text,
  `thumbnail` text,
  `owner` varchar(255),
  `attributes` text,
  `status` char(1) not null default '0',
  `privilege` char(1) not null default '1',
  `is_published` boolean not null default false,
  `created` timestamp default now(),
  `updated` timestamp default now()
);

create table `attachment` (
  `id` bigint unsigned auto_increment not null primary key,
  `content_id` bigint unsigned not null references content(id) on update cascade on delete cascade,
  `display_name` varchar(255) not null,
  `filepath` varchar(255) not null,
  `created` timestamp default now()
);

create table `content_category` (
  `content_id` bigint unsigned not null references content(id) on update cascade on delete cascade,
  `category_id` int unsigned not null references category(id) on update cascade on delete cascade
);

# --- !Downs

drop table if exists `content_category`;
drop table if exists `attachment`;
drop table if exists `content`;
drop table if exists `category`;
