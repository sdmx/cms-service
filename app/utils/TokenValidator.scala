package utils

import java.util.Base64

import better.files.File
import io.jsonwebtoken.{Claims, Jws, JwtException, Jwts}
import javax.inject.{Inject, Singleton}
import play.api.mvc.{AnyContent, BodyParsers, Request}
import play.api.{Configuration, Logger}

import scala.concurrent.ExecutionContext

@Singleton
class TokenValidator@Inject()(
  config: Configuration,
) {

  def validate(token: String) = decode(token).isDefined

  def decode(token: String): Option[Jws[Claims]] = {
    try {
      if (token.nonEmpty) {
        val secretKey = Base64.getEncoder.encodeToString(config.get[String]("play.http.secret.key").getBytes)
        Some(Jwts.parser.setSigningKey(secretKey).parseClaimsJws(token))
      }
      else None
    }
    catch {
      case e: JwtException =>
        Logger.error(e.getMessage)
        None
    }
  }

  def getTokenFromRequest[A](request: Request[A]): String = {
    request.headers.get("Authorization").getOrElse("").replace("Bearer ", "")
  }

  def getClaimsFromRequest[A](request: Request[A]) = decode(getTokenFromRequest(request))
}
