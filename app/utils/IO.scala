package utils

import better.files.File

object IO {
  val storage = File("soenda")

  def extension(filename: String) = filename.substring(filename.lastIndexOf('.') + 1)

  def delete(filepath: String): Boolean = {
    val file = storage/filepath
    val exists = file.exists

    if (exists) file.delete()

    exists
  }
}
