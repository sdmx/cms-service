package utils

import java.util.Base64

import io.jsonwebtoken._
import play.api.mvc._
import play.api._

object TokenManager {

  def validate(token: String) = decode(token).isDefined

  def decode(token: String): Option[Jws[Claims]] = {
    try {
      if (token.nonEmpty) {
        Some(Jwts.parser.setSigningKey(secretKey).parseClaimsJws(token))
      }
      else None
    }
    catch {
      case e: JwtException =>
        Logger.error(e.getMessage)
        None
    }
  }

  def secretKey = Base64.getEncoder.encodeToString{
    play.Play.application().configuration().getString("play.http.secret.key").getBytes
  }

  def getTokenFromRequest[A](request: Request[A]): String = {
    request.headers.get("Authorization").getOrElse("").replace("Bearer ", "")
  }

  def getClaimsFromRequest[A](request: Request[A]) = decode(getTokenFromRequest(request))
}
