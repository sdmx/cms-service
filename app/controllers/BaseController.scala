package controllers

import controllers.actions.AuthenticatedRequest
import play.api.mvc._
import utils.TokenManager

abstract class BaseController(val cc: ControllerComponents) extends AbstractController(cc) {
  def Authenticated[A](action: Action[A]) = Action.async(action.parser) { request =>
    action(new AuthenticatedRequest(TokenManager.getClaimsFromRequest(request), request))
  }

  def getUid[A](request: Request[A]) = {
    val jws = TokenManager.getClaimsFromRequest(request)
    jws.fold("")(jws => jws.getBody.get("uid").toString)
  }
}
