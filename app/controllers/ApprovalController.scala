package controllers

import scala.concurrent._
import scala.concurrent.duration._

import org.joda.time.DateTime

import com.softwaremill.sttp._

import controllers.traits.FormController
import javax.inject._
import models._
import play.api._
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.mvc.{Request, _}
import play.api.data.{Form, FormError}


@Singleton
class ApprovalController @Inject()(
  cc: ControllerComponents,
  config: Configuration,
  messagesAction: MessagesActionBuilder,
  approvalRepo: ApprovalRepository,
  contentRepo: ContentRepository,
  manifestRepo: ApprovalManifestRepository,
  flowRepo: ApprovalFlowRepository,
  flowDetailRepo: ApprovalFlowDetailRepository,
) (implicit ec: ExecutionContext) extends BaseController(cc)
  with FormController[ApprovalForm]
  with models.traits.OptionJsonFormat
  with models.traits.DateTimeJsonFormat {

  implicit val backend = HttpURLConnectionBackend()

  def list = Authenticated {
    Action.async { implicit request =>
      val limit = Some(request.getQueryString("limit").fold(100L)(_.toLong))
      val offset = Some(request.getQueryString("offset").fold(0L)(_.toLong))

      approvalRepo
        .listManifested(getUid(request), limit, offset)
        .map { data => Ok(data.asJson) }
    }
  }

  def retrieve(id: Long) = Authenticated {
    Action.async { implicit request =>
      approvalRepo.findWithContent(id).map {
        case Some((approval, content, categories, attachments)) =>
          Ok(Json.toJson(JsObject(Seq(
            "approval" -> Json.toJson(approval),
            "content" -> Json.toJson(content),
            "categories" -> Json.toJson(categories),
            "attachments" -> Json.toJson(attachments),
          ))))
        case None => NotFound
      }
    }
  }

  // people submit approval to their boss, following their flow configuration
  def submit(_type: String) = Authenticated {
    messagesAction.async { implicit request =>
      submitApproval { (authInfo: AuthInfo, data: ApprovalForm) =>
        val contentId = request.body.asFormUrlEncoded.getOrElse(Map())
          .getOrElse("content_id", Seq("-1")).headOption.get
          .toLong

        approvalRepo.submit(authInfo, ContentType.getCodeByName(_type), contentId)
      }
    }
  }

  // people with higher position, approve the submission
  def next(id: Long, action: String) = Authenticated {
    Action(parse.multipartFormData).async { implicit request =>
      getAuthInfo(utils.TokenManager.getTokenFromRequest(request)).validate[AuthInfo] match {
        case s: JsSuccess[AuthInfo] =>
          val formData = request.body.asFormUrlEncoded

          ContentForm.form.bindFromRequest.fold(
            (error: Form[ContentForm]) => {
              implicit val formErrorWrites: Writes[FormError] = (error: FormError) => {
                Json.obj("key" -> error.key, "message" -> error.messages)
              }

              Future.successful(BadRequest(Json.toJson(error.errors)))
            },
            contentData => {
              val auth = s.get
              val statusUpdate = action match {
                case "approve" => Approval.Status.approved
                case "reject" => Approval.Status.rejected
                case _ => Approval.Status.waiting
              }

              for {
                approvalRes <- approvalRepo.find(id)
                contentRes <- contentRepo.find(approvalRes.get.contentId)
                manifestRes <- manifestRepo.findByApprovalId(id)
                if contentRes.isDefined && approvalRes.isDefined
              } yield {
                val approval = approvalRes.get
                val contentOri = contentRes.get

                manifestRes
                  .find(m => /*m.status == Approval.Status.waiting && */m.uid.getOrElse("") == auth.uid)
                  .map { selManifest =>
                    val files = for (file <- request.body.files if file.key == "files") yield file
                    val thumbnail = request.body.file("thumbnail")
                    var content = contentData.asModel(Some(contentOri))
                    val isEdited = !content.equals(contentOri) || files.length > 0 || thumbnail.isDefined

                    // update manifest
                    val currentManifest = selManifest.copy(
                      status = statusUpdate,
                      isEdited = isEdited,
                      notes = formData.getOrElse("notes", Seq()).headOption,
                    )

                    manifestRepo.update(currentManifest.id.getOrElse(0), currentManifest)

                    println("#STATUS", statusUpdate)
                    statusUpdate match {
                      case Approval.Status.rejected =>
                        // update approval status
                        approvalRepo.update(id, approval.copy(status = statusUpdate))
                        content = content.copy(
                          status = Content.Status.rejected,
                          isPublished = false
                        )

                        println("#REJECTED", content)

                      case Approval.Status.approved =>
                        // find next approval
                        var isMandatory: Boolean = false
                        val nextManifests = manifestRes.map { manifest =>
                          if (manifest.uid.isDefined
                            && manifest.orderNum > currentManifest.orderNum
                            && !isMandatory) {
                            val updatedManifest = manifest.copy(
                              status = Approval.Status.waiting,
                              updated = DateTime.now(),
                            )
                            isMandatory = updatedManifest.isMandatory

                            Some(updatedManifest)
                          }
                          else None
                        }.filter(_.isDefined).map(_.get)

                        // define manifest status to 'waiting' for the next approvals
                        if (nextManifests.length > 0) {
                          nextManifests.map(manifest =>
                            manifestRepo.update(manifest.id.getOrElse(0), manifest)
                          )
                        }
                        // approval accepted
                        else {
                          // publish content
                          content = content.copy(
                            status = Content.Status.approved,
                            isPublished = true
                          )

                          // set approval status
                          approvalRepo.update(id, approval.copy(status = statusUpdate))
                        }
                    }

                    // update content into database if there is an update
                    if (isEdited || content != contentOri) {
                      contentRepo.update(
                        contentOri.id.getOrElse(0),
                        content,
                        Some(files),
                        request.body.file("thumbnail"),
                        contentData.categoryIds
                      )
                    }
                  }

                Ok
              }
            }
          )

        case e: JsError => Future(Unauthorized)
      }
    }
  }

  def submitApproval[A](callback: (AuthInfo, ApprovalForm) => Unit)(implicit request: Request[A]) = {
    ApprovalForm.form.bindFromRequest.fold(handleFormError, data => {
      getAuthInfo(utils.TokenManager.getTokenFromRequest(request)).validate[AuthInfo] match {
        case s: JsSuccess[AuthInfo] =>
          callback(s.get, data)
          Future(Ok)
        case e: JsError =>
          Future(Unauthorized)
      }
    })
  }

  def getContentType(_type: String) = ContentType.types.getOrElse(_type, 'x')

  def getUserByPosition(orgId: Long, posIds: Seq[Long]) = {
    val endpoint = config.get[String]("client.auth.endpoint")
    val body = Seq(("org_id", orgId.toString)) ++ posIds.map(posId => ("pos_id[]", posId.toString))
    val res = sttp.body(body : _*).post(uri"$endpoint/user/by-position").send()

    Json.parse(res.body.fold(x => x, x => x))
  }

  def getAuthInfo(token: String) = {
    val endpoint = config.get[String]("client.auth.endpoint")
    val res = sttp.get(uri"$endpoint/auth/info/$token").send()

    Json.parse(res.body.fold(x => x, x => x))
  }
}
