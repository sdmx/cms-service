package controllers

import javax.inject._
import models._
import play.api.data.{Form, FormError}
import play.api.libs.json._
import play.api.mvc._

import scala.concurrent._

@Singleton
class CategoryController @Inject()(
  cc: ControllerComponents,
  categoryRepo: CategoryRepository,
) (implicit ec: ExecutionContext) extends BaseController(cc) {

  def list = Action.async { implicit request =>
    val limit = Some(request.getQueryString("limit").fold(100L)(_.toLong))
    val offset = Some(request.getQueryString("offset").fold(0L)(_.toLong))

    categoryRepo
      .list(request.getQueryString("q"), limit, offset)
      .map { data => Ok(data.asJson) }
  }

  def retrieve(id: Long) = Action.async {
    categoryRepo.find(id).map(category => if (category.isDefined) Ok(Json.toJson(category.get)) else NotFound)
  }

  def create = Action.async { implicit request =>
    CategoryForm.form.bindFromRequest.fold(
      handleFormError,
      data => {
        val category = Category(None, data.name, data.parentId)
        categoryRepo.create(category).map(_ => Ok)
      }
    )
  }

  def update(id: Long) = Action.async { implicit request =>
    CategoryForm.form.bindFromRequest.fold(
      handleFormError,
      data => {
        categoryRepo.find(id).map(category => {
          if (category.isDefined) {
            categoryRepo.update(id, category.get.copy(
              name = data.name,
              parentId = data.parentId
            )).synchronized(Ok)
          }
          else NotFound
        })
      }
    )
  }

  def destroy(id: Long) = Action.async {
    categoryRepo.delete(id).map(status => if (status == 0) NotFound else Ok)
  }

  private def handleFormError = (error: Form[CategoryForm]) => {
    implicit val formErrorWrites: Writes[FormError] = (error: FormError) => {
      Json.obj("key" -> error.key, "message" -> error.messages)
    }

    Future.successful(Ok(Json.toJson(error.errors)))
  }
}
