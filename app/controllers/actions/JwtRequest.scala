package controllers.actions

import io.jsonwebtoken._
import javax.inject.Inject
import play.api.mvc._
import play.api._
import utils.TokenValidator

import scala.concurrent._

class AuthenticatedRequest[A](val jws: Option[Jws[Claims]], request: Request[A]) extends WrappedRequest[A](request)

class JwtRequest @Inject()(
  val parser: BodyParsers.Default,
  config: Configuration,
  tokenValidator: TokenValidator,
)(implicit val executionContext: ExecutionContext)
  extends ActionBuilder[AuthenticatedRequest, AnyContent]
  with ActionTransformer[Request, AuthenticatedRequest] {

  def transform[A](request: Request[A]) = Future.successful {
    new AuthenticatedRequest(tokenValidator.getClaimsFromRequest(request), request)
  }
}