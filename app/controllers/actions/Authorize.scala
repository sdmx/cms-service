package controllers.actions

import play.api.libs.json.JsObject
import play.api.mvc._

import scala.concurrent._

//class Authorize @Inject() (parser: BodyParsers.Default)(implicit ec: ExecutionContext) extends ActionBuilderImpl(parser) {
//  override def invokeBlock[A](request: Request[A], block: Request[A] => Future[Result]) = {
//    val token = request.headers.get("Authorization").getOrElse("").replace("Bearer ", "")
//    if (true) Future.successful(Results.Forbidden) else block(request)
//  }
//}

object Authorize {
  def async[T](request: Request[T], user: Option[JsObject]) = {
    block: Future[Result] => new Authorize[T](request, user, block)
  }
  def apply[T](request: Request[T], user: Option[JsObject])(implicit ec: ExecutionContext) = {
    block: Result => new Authorize[T](request, user, future { block })
  }
}

class Authorize[T](request: Request[T], user: Option[JsObject], success: Future[Result]) {
  def authorised = user.isDefined
//  def otherwise(block: Future[Result]) : Future[Result] = authorised.flatMap { valid => if (valid) success else block }
//  def otherwise(block: Result) : Result = if(authorised.value.get.get) success.value.get.get else block
}