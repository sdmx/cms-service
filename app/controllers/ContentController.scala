package controllers

import controllers.traits.FormController
import javax.inject._
import models._
import play.api._
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.mvc.{Request, _}
import play.api.data.{Form, FormError}
import com.softwaremill.sttp._

import scala.concurrent._
import scala.concurrent.duration.Duration

@Singleton
class ContentController @Inject()(
  cc: ControllerComponents,
  config: Configuration,
  messagesAction: MessagesActionBuilder,
  contentRepo: ContentRepository,
  contentCategoryRepo: ContentCategoryRepository,
  attachmentRepo: AttachmentRepository,
  approvalRepo: ApprovalRepository
) (implicit ec: ExecutionContext) extends BaseController(cc) with FormController[ContentForm] {
  implicit val backend = HttpURLConnectionBackend()

  def list(contentType: String, privilege: String) = ContentAction(contentType) {
    Action.async { implicit request =>
      val reqLimit = request.getQueryString("limit")
      val limit = if (reqLimit.isDefined) Some(reqLimit.get.toLong) else Some(100L)

      val reqOffset = request.getQueryString("offset")
      val offset = if (reqOffset.isDefined) Some(reqOffset.get.toLong) else Some(0L)

      val uid = getUid(request)
      val optUid = if (uid.length > 0) Some(uid) else None

      contentRepo
        .list(contentType, privilege, request.getQueryString("q"), limit, offset, None, optUid)
        .map { data => Ok(data.asJson) }
    }
  }

  def retrieve(contentType: String, privilege: String, id: Long) = Action.async {
    contentRepo.find(id).map(content =>
      if (content.isDefined) {
        val attachments = Await.result(attachmentRepo.getByContent(id), Duration.Inf)
        val categories = Await.result(contentCategoryRepo.getCategories(id), Duration.Inf)

        Ok(JsObject(Seq(
          "content" -> Json.toJson(content.get),
          "categories" -> Json.toJson(categories),
          "attachments" -> Json.toJson(attachments)
        )))
      }
      else NotFound
    )
  }

  def create(contentType: String, privilege: String) = messagesAction(parse.multipartFormData).async { implicit request =>
    ContentForm.form.bindFromRequest.fold(handleFormError, data => {
      getAuthInfo(utils.TokenManager.getTokenFromRequest(request)).validate[AuthInfo] match {
        case e: JsError =>
          Logger.error(e.toString)
          Future(Unauthorized)

        case s: JsSuccess[AuthInfo] =>
          val auth = s.get
          val contentTypeCode = ContentType.getCodeByName(contentType)
          val privilegeCode = Content.Privilege.getCodeByName(privilege)
          val files = for (file <- request.body.files if file.key == "files") yield file
          val model = data.asModel(None, contentTypeCode, privilegeCode, auth.uid)

          contentRepo
            .create(model, Some(files), request.body.file("thumbnail"), data.categoryIds)
            .map { content =>
              // submit content to approval flow
              if (data.submit.getOrElse(false))
                approvalRepo.submit(auth, contentTypeCode, content.id.getOrElse(0))
            }

          Future(Ok)
      }
    })
  }

  def update(contentType: String, privilege: String, id: Long) = messagesAction(parse.multipartFormData).async { implicit request =>
    ContentForm.form.bindFromRequest.fold(handleFormError, data => {
      contentRepo.find(id).map {
        case Some(content) =>
          getAuthInfo(utils.TokenManager.getTokenFromRequest(request)).validate[AuthInfo] match {
            case s: JsSuccess[AuthInfo] =>
              val auth = s.get
              val files = for (file <- request.body.files if file.key == "files") yield file
              val contentTypeCode = ContentType.getCodeByName(contentType)

              contentRepo.update(
                id,
                data.asModel(Some(content), contentTypeCode),
                Some(files),
                request.body.file("thumbnail"),
                data.categoryIds
              )

              // submit content to approval flow
              if (data.submit.getOrElse(false))
                approvalRepo.submit(auth, contentTypeCode, content.id.getOrElse(0))

              Ok

            case e: JsError =>
              Logger.error(e.toString)
              Unauthorized
          }

        case None => NotFound
      }
    })
  }

  def destroy(contentType: String, privilege: String, id: Long) = Action.async {
    contentRepo.delete(id).map(status => if (status == 1) Ok else NotFound)
  }

  def uploadAttachment(contentType: String, privilege: String, id: Long) = Action(parse.multipartFormData).async { implicit request =>
    contentRepo.find(id).map(content =>
      if (content.isDefined) {
        attachmentRepo.attachFiles(content.get, request.body.files)
        Ok
      }
      else NotFound
    )
  }

  def removeAttachment(contentType: String, privilege: String, id: Long, attachmentId: Long) = Action.async {
    contentRepo.find(id).map(content =>
      if (content.isDefined) {
        attachmentRepo.delete(attachmentId)
        Ok
      }
      else NotFound
    )
  }

  case class ContentAction[A](contentType: String)(action: Action[A]) extends Action[A] {
    override def parser = action.parser
    override def executionContext = action.executionContext

    def apply(request: Request[A]): Future[Result] = {
      if (ContentType.types.contains(contentType)) action(request) else Future(NotFound)
    }
  }

  def getAuthInfo(token: String) = {
    val endpoint = config.get[String]("client.auth.endpoint")
    val res = sttp.get(uri"$endpoint/auth/info/$token").send()

    Json.parse(res.body.fold(x => x, x => x))
  }
}
