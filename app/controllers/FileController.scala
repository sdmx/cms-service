package controllers

import akka.stream.scaladsl._
import javax.inject._
import play.api.http.HttpEntity
import play.api.mvc._
import utils.IO

import scala.concurrent.ExecutionContext

@Singleton
class FileController @Inject()(
  cc: ControllerComponents,
)(implicit ec: ExecutionContext) extends BaseController(cc) {

  def download(fileuri: String) = Action {
    val file = IO.storage/fileuri

    if (file.exists) {
      val contentType = if (file.contentType.isDefined) file.contentType.get else "application/octet-stream"

      Result(
        header = ResponseHeader(200, Map(
          "Content-Type" -> s"$contentType; charset=utf-8",
          "Content-Transfer-Encoding" -> "binary",
          "Cache-control" -> "private",
          "Pragma" -> "private",
          "Expires" -> "0",
          "Content-Length" -> s"${file.size}",
          "Content-Disposition" -> s"attachment; filename=${file.name}"
        )),
        body = HttpEntity.Streamed(FileIO.fromPath(file.path), None, Some(contentType))
      )
    }
    else NotFound
  }

  def file(fileuri: String) = Action {
    val file = IO.storage/fileuri

    if (file.exists) {
      val contentType = if (file.contentType.isDefined) file.contentType.get else "application/octet-stream"

      Result(
        header = ResponseHeader(200, Map(
          "Content-Type" -> s"$contentType; charset=utf-8",
          "Content-Transfer-Encoding" -> "binary",
          "Cache-control" -> "private",
          "Pragma" -> "private",
          "Expires" -> "0",
          "Content-Length" -> s"${file.size}",
          "Content-Disposition" -> s"inline"
        )),
        body = HttpEntity.Streamed(FileIO.fromPath(file.path), None, Some(contentType))
      )
    }
    else NotFound
  }
}
