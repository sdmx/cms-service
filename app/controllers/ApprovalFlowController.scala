package controllers

import controllers.traits.FormController
import javax.inject._
import models._
import play.api.libs.json._
import play.api.mvc._

import scala.concurrent._
import scala.concurrent.duration.Duration

@Singleton
class ApprovalFlowController @Inject()(
  cc: ControllerComponents,
  flowRepo: ApprovalFlowRepository,
  flowDetailRepo: ApprovalFlowDetailRepository,
)(implicit ec: ExecutionContext) extends BaseController(cc) with FormController[ApprovalFlowForm] {

  def list = Authenticated {
    Action.async { implicit request =>
      flowRepo
        .list(getUid(request))
        .map { data => Ok(data.asJson) }
    }
  }

  def retrieve(posId: Long) = Authenticated {
    Action.async { implicit request =>
      flowRepo.findByUserPosition(getUid(request), posId).map { dataFlow =>
        val types = ContentType.types.map(_.swap)
        val res = for (flow <- dataFlow) yield {
          flow.id match {
            case None => None
            case Some(id) =>
              val details = Await.result(flowDetailRepo.findByFlowId(id), Duration.Inf)

              Some(JsObject(Seq(
                "type" -> JsString(types.getOrElse(flow.contentType, "unknown")),
                "flow" -> Json.toJson(flow),
                "details" -> Json.toJson(details)
              )))
          }
        }

        Ok(Json.toJson(res))
      }
    }
  }

  def create(_type: String) = Authenticated {
    Action.async { implicit request =>
      ApprovalFlowForm.form.bindFromRequest.fold(handleFormError, data => {
        flowRepo.findOrCreate(ApprovalFlow(
          uid = getUid(request),
          contentType = contentType(_type),
          orgId = data.orgId,
          posId = data.posId
        )).map { _.id match {
          case Some(id) =>
            flowDetailRepo.deleteByFlowId(id)
            flowDetailRepo.attachDetails{
              for ((posId, i) <- data.flowPositionIds.zipWithIndex) yield {
                ApprovalFlowDetail(
                  approvalFlowId = id,
                  orgId = data.flowOrganisationIds(i),
                  posId = posId,
                  orderNum = i,
                  isMandatory = data.flowIsMandatory(i)
                )
              }
            }
            Ok
          case _ => NotFound
        }}
      })
    }
  }

  def destroy(_type: String) = Authenticated {
    Action.async { implicit request =>
      flowRepo.findByType(contentType(_type), getUid(request)).map {
        case Some(flow) =>
          flow.id match {
            case Some(id) =>
              flowDetailRepo.deleteByFlowId(id)
              flowRepo.delete(id)
              Ok
            case _ => NotFound
          }
        case _ => NotFound
      }
    }
  }

  def contentType(_type: String) = ContentType.types.getOrElse(_type, 'x')
}
