package controllers.traits

import play.api.data.{Form, FormError}
import play.api.libs.json.{Json, Writes}
import play.api.mvc.AbstractController

import scala.concurrent.Future

trait FormController[A] extends AbstractController {
  protected def handleFormError = (error: Form[A]) => {
    implicit val formErrorWrites: Writes[FormError] = (error: FormError) => {
      Json.obj("key" -> error.key, "message" -> error.messages)
    }

    Future.successful(BadRequest(Json.toJson(error.errors)))
  }
}
