package controllers

import javax.inject._

import play.api.mvc._
import models._
import scala.concurrent._
import utils._

@Singleton
class OrganisationController @Inject()(
  cc: ControllerComponents,
  orgRepo: OrganisationRepository,
  posRepo: PositionRepository,
)(implicit ec: ExecutionContext) extends BaseController(cc) {
  def list = Action.async {
    orgRepo.list().map { data => Ok(data.asJson) }
  }

  def positions = Action.async { implicit request =>
    posRepo.list().map { data => Ok(data.asJson) }
    // TokenManager.getClaimsFromRequest(request) match {
    //   case Some(claims) =>
    //     // println(claims.getBody.get("uid"))
    //     posRepo.list().map { data => Ok(data.asJson) }
    //   case None => Future(Unauthorized)
    // }
  }
}
