package models

import java.sql.Timestamp

import javax.inject._
import org.joda.time.DateTime
import play.api.db.slick.DatabaseConfigProvider
import play.api.libs.Files.TemporaryFile
import play.api.libs.json._
import play.api.mvc.MultipartFormData.FilePart
import slick.jdbc.JdbcProfile
import utils.IO

import scala.concurrent.duration.Duration
import scala.concurrent._

@Singleton
class ContentRepository @Inject()(
  dbConfigProvider: DatabaseConfigProvider,
  attachmentRepo: AttachmentRepository,
  categoryRepo: CategoryRepository,
  contentCategoryRepo: ContentCategoryRepository,
)(implicit ec: ExecutionContext) {
  protected val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  implicit def dateTimeMapper =
    MappedColumnType.base[DateTime, Timestamp](
      dt => new Timestamp(dt.getMillis),
      ts => new DateTime(ts.getTime)
    )

  implicit def attributesMapper = MappedColumnType.base[JsValue, String](
    attributes => Json.stringify(attributes),
    jsonString => Json.parse(jsonString)
  )

  implicit def listMapper = MappedColumnType.base[List[String], String](
    list => list.mkString(","),
    links => links.split("\\,").toList
  )

  class ContentTable(tag: Tag) extends Table[Content](tag, "content") {
    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)
    def contentType = column[String]("content_type")
    def name = column[String]("name")
    def body = column[String]("body")
    def owner = column[String]("owner")
    def thumbnail = column[Option[String]]("thumbnail")
    def links = column[Option[List[String]]]("links")
    def attributes = column[Option[JsValue]]("attributes")
    def isPublished = column[Boolean]("is_published")
    def status = column[Char]("status")
    def privilege = column[Char]("privilege")
    def created = column[DateTime]("created")
    def updated = column[DateTime]("updated")

    def * = (
      id,
      contentType,
      name,
      body,
      owner,
      thumbnail,
      links,
      attributes,
      isPublished,
      status,
      privilege,
      created,
      updated,
    ) <> ((Content.apply _).tupled, Content.unapply)
  }

  val table = TableQuery[ContentTable]

  def create(o: Content, files: Option[Seq[FilePart[TemporaryFile]]]): Future[Content] = {
    db
      .run(table returning table.map(_.id) into ((item, id) => item.copy(id = id)) += o)
      .map { item =>
        if (files.isDefined) {
          Await.result(attachmentRepo.attachFiles(item, files.get), Duration.Inf)
        }

        item
      }
  }

  def create(
    content: Content,
    files: Option[Seq[FilePart[TemporaryFile]]],
    thumbnail: Option[FilePart[TemporaryFile]],
    categoryIds: Option[Seq[Long]]
  ): Future[Content] = {
    create(content.copy(thumbnail = uploadThumbnail(thumbnail, content.contentType)), files)
      .map(content => {
        // mapping content into categories
        if (content.id.isDefined && categoryIds.isDefined) {
          contentCategoryRepo.create(content.id.get, categoryIds.getOrElse(Seq()))
        }

        content
      })
  }

  def find(id: Long): Future[Option[Content]] = {
    db.run(table.filter(_.id === id).result.headOption)
  }

  def update(id: Long, o: Content, files: Option[Seq[FilePart[TemporaryFile]]]): Future[Int] = {
    db.run(table.filter(_.id === id).update(o)).map { status =>
      files.map(attachFiles => attachmentRepo.attachFiles(o, attachFiles))
      status
    }
  }

  def update(
    id: Long,
    content: Content,
    files: Option[Seq[FilePart[TemporaryFile]]],
    thumbnail: Option[FilePart[TemporaryFile]],
    categoryIds: Option[Seq[Long]]
  ): Future[Int] = {
    // replace old thumbnail
    val _thumbnail = thumbnail match {
      case Some(file) =>
        uploadThumbnail(
          thumbnail,
          content.thumbnail.getOrElse(content.contentType),
          content.thumbnail.isDefined
        )
      case None => content.thumbnail
    }

    // update to database
    update(id, content.copy(thumbnail = _thumbnail), files).map { status =>
      contentCategoryRepo.recreate(id, categoryIds.getOrElse(Seq()))
      status
    }
  }

  // def update(
  //   id: Long,
  //   contentType: String,
  //   form: ContentForm,
  //   files: Option[Seq[FilePart[TemporaryFile]]],
  //   thumbnail: Option[FilePart[TemporaryFile]]
  // ): Future[Int] = {
  //   find(id).map(content => {
  //     if (content.isDefined) {
  //       val _attributes = if (form.attributes.isDefined) Some(Json.parse(form.attributes.get)) else content.get.attributes
  //       val _links = if (form.links.isDefined) Some(form.links.get.split("\\,").toList) else content.get.links
  //       val _status = if (form.status.isDefined) form.status.get else content.get.status
  //       val _privilege = if (form.privilege.isDefined) form.privilege.get else content.get.privilege

  //       // replace old thumbnail
  //       val oldThumbnail = content.get.thumbnail
  //       val _thumbnail =
  //         if (thumbnail.isDefined)
  //           uploadThumbnail(
  //             thumbnail,
  //             oldThumbnail.getOrElse(getContentTypeCode(contentType)),
  //             oldThumbnail.isDefined
  //           )
  //         else oldThumbnail

  //       // update to database
  //       update(id, content.get.copy(
  //         name = form.name,
  //         body = form.body,
  //         isPublished = form.isPublished,
  //         attributes = _attributes,
  //         links = _links,
  //         status = _status,
  //         privilege = _privilege,
  //         thumbnail = _thumbnail
  //       ), files)
  //         .map(_ =>
  //           // re-mapping content into categories
  //           if (form.categoryIds.isDefined) {
  //             contentCategoryRepo.recreate(id, form.categoryIds.get)
  //           }
  //         )

  //       1
  //     }
  //     else 0
  //   })
  // }

  def delete(id: Long): Future[Int] = {
    find(id).map(data => {
      if (data.isDefined) {
        // delete attachments
        attachmentRepo.deleteByContent(id)

        // remove thumbnail file
        if (data.get.thumbnail.isDefined)
          IO.delete(data.get.thumbnail.get)

        // delete master data
        db.run(table.filter(_.id === id).delete)
        1
      }
      else 0
    })
  }

  def list(
    contentType: String,
    privilege: String,
    q: Option[String] = None,
    limit: Option[Long] = Some(100L),
    offset: Option[Long] = Some(0L),
    sort: Option[Seq[(String, String)]] = None,
    owner: Option[String] = None
  ): Future[Pagination[Content]] = db.run {
    var query = table.filter(i =>
      i.contentType === getContentTypeCode(contentType)
        && i.privilege === Content.Privilege.getCodeByName(privilege)
    )

    owner match {
      case Some(uid) => query = query.filter(_.owner === uid)
      case None => query = query.filter(_.isPublished === true)
    }

    // data sorting
    query = query.sortBy(_.id.desc)

    // filter by key
    if (q.isDefined)
      query = query.filter(row => row.name.like(s"%${q.get}%") || row.body.like(s"%${q.get}%"))

    // total results
    val totalCount = Await.result(db.run(query.length.result), Duration.Inf)

    // pagination
    if (offset.isDefined)
      query = query.drop(offset.get)

    if (limit.isDefined)
      query = query.take(limit.get)

    query.result.map(data => new Pagination(data, totalCount, limit, offset))
  }

  private def uploadThumbnail(
    filepart: Option[FilePart[TemporaryFile]],
    path: String,
    replace: Boolean = false
  ): Option[String] = {
    if (filepart.isDefined) {
      val allowedExtensions = Array("png", "jpg", "jpeg", "gif")
      val ext = IO.extension(filepart.get.filename).toLowerCase

      // invalid extension
      if (!allowedExtensions.contains(ext))
        return None

      // upload thumbnail image
      val fileuri = if (!replace) s"$path/${java.util.UUID.randomUUID}.$ext" else path
      val file = (IO.storage/fileuri).createIfNotExists(false, true)
      // TODO: resize thumbnail
      filepart.get.ref.moveTo(file.path, true)

      Some(fileuri)
    }
    else None
  }

  private def getContentTypeCode(contentType: String) = {
    ContentType.types.getOrElse(contentType, 'x').toString
  }
}