package models

import javax.inject._
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

@Singleton
class OrganisationRepository @Inject()(dbConfigProvider: DatabaseConfigProvider) (implicit ec: ExecutionContext) {
  protected val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  class OrganisationTable(tag: Tag) extends Table[Organisation](tag, "organisation") {
    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)
    def parentId = column[Option[Long]]("parent_id")
    def name = column[String]("name")
    def orgType = column[Char]("type")
    def * = (id, parentId, name, orgType) <> ((Organisation.apply _).tupled, Organisation.unapply)
  }

  val table = TableQuery[OrganisationTable]

  def create(o: Organisation): Future[Organisation] = {
    db.run(table returning table.map(_.id) into ((item, id) => item.copy(id = id)) += o)
  }

  def find(id: Long): Future[Option[Organisation]] = {
    db.run(table.filter(_.id === id).result.headOption)
  }

  def update(id: Long, o: Organisation): Future[Organisation] = {
    db.run(table.filter(_.id === id).update(o)).map(_ => o)
  }

  def delete(id: Long): Future[Int] = {
    db.run(table.filter(_.id === id).delete)
  }

  def list(
    q: Option[String] = None,
    limit: Option[Long] = None,
    offset: Option[Long] = None,
    sort: Option[Seq[(String, String)]] = None
  ): Future[Pagination[Organisation]] = db.run {
    var query = table.sortBy(_.id.desc)

    // filter by key
    if (q.isDefined)
      query = table.filter(_.name.like(s"%${q.get}%"))

    // total results
    val totalCount = Await.result(db.run(query.length.result), Duration.Inf)

    // pagination
    if (offset.isDefined)
      query = query.drop(offset.get)

    if (limit.isDefined)
      query = query.take(limit.get)

    query.result.map(data => new Pagination(data, totalCount, limit, offset))
  }
}