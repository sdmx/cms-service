package models

import java.sql.Timestamp

import javax.inject._
import org.joda.time.DateTime
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile
import com.softwaremill.sttp._
import play.api.libs.json._
import play.api.{Configuration, Logger}
import play.api.libs.mailer._
import play.api.mvc.MessagesRequestHeader

import scala.concurrent.duration.Duration
import scala.concurrent._
import models._

@Singleton
class ApprovalRepository @Inject()(
  dbConfigProvider: DatabaseConfigProvider,
  config: Configuration,
  mail: MailerClient,
  contentRepo: ContentRepository,
  categoryRepo: CategoryRepository,
  contentCategoryRepo: ContentCategoryRepository,
  attachmentRepo: AttachmentRepository,
  flowRepo: ApprovalFlowRepository,
  flowDetailRepo: ApprovalFlowDetailRepository,
  manifestRepo: ApprovalManifestRepository,
) (implicit ec: ExecutionContext) {
  protected val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._
  implicit val backend = HttpURLConnectionBackend()

  implicit def dateTimeMapper =
    MappedColumnType.base[DateTime, Timestamp](
      dt => new Timestamp(dt.getMillis),
      ts => new DateTime(ts.getTime)
    )

  class ApprovalTable(tag: Tag) extends Table[Approval](tag, "approval") {
    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)
    def uid = column[String]("uid")
    def contentType = column[Char]("content_type")
    def contentId = column[Long]("content_id")
    def status = column[Char]("status")
    def created = column[DateTime]("created")
    def * = (
      id,
      uid,
      contentType,
      contentId,
      status,
      created,
    ) <> ((Approval.apply _).tupled, Approval.unapply)
  }

  val table = TableQuery[ApprovalTable]

  def create(o: Approval): Future[Approval] = {
    db.run(table returning table.map(_.id) into ((item, id) => item.copy(id = id)) += o)
  }

  def find(id: Long): Future[Option[Approval]] = {
    db.run(table.filter(_.id === id).result.headOption)
  }

  def findWithContent(id: Long) = {
    var query = for {
      ((((approval, content), contentCategory), category), attachment) <-
        table.filter(_.id === id)
          .join(contentRepo.table).on(_.contentId === _.id)
          .joinLeft(contentCategoryRepo.table).on(_._2.id === _.contentId)
          .joinLeft(categoryRepo.table).on(_._2.map(_.categoryId) === _.id)
          .joinLeft(attachmentRepo.table).on((x, a) => x._1._1._2.id === a.contentId)
    } yield (approval, content, category, attachment)

    db.run(query.result).map { res =>
      res.map(_._1).headOption.map { approval => (
        approval,
        res.map(_._2).headOption,
        res.map(_._3).filter(!_.isEmpty).distinct,
        res.map(_._4).filter(!_.isEmpty).distinct,
      )}
    }
  }

  def update(id: Long, o: Approval): Future[Approval] = {
    db.run(table.filter(_.id === id).update(o)).map(_ => o)
  }

  def delete(id: Long): Future[Int] = db.run(table.filter(_.id === id).delete)

  def list(
    uid: String,
    limit: Option[Long] = Some(100L),
    offset: Option[Long] = Some(0L),
    sort: Option[Seq[(String, String)]] = None
  ): Future[Pagination[(Approval, Option[Content])]] = db.run {
    var query = for {
      (a, c) <- table.sortBy(_.id.desc).filter(_.uid === uid)
        .joinLeft(contentRepo.table) on (_.contentId === _.id)
    } yield (a, c)

    // total results
    val totalCount = Await.result(db.run(query.length.result), Duration.Inf)

    // pagination
    if (offset.isDefined)
      query = query.drop(offset.get)

    if (limit.isDefined)
      query = query.take(limit.get)

    query.result.map(data => new Pagination(data, totalCount, limit, offset))
  }

  def listManifested(
    uid: String,
    limit: Option[Long] = Some(100L),
    offset: Option[Long] = Some(0L),
    sort: Option[Seq[(String, String)]] = None
  ): Future[Pagination[(Approval, Content)]] = db.run {
    var query = for {
      // (_, approval) <- table.sortBy(_.id.desc).filter(_.uid === uid)
      //   .joinLeft(approvalRepo.table) on (_.approvalId === _.id)
      manifest <-
        manifestRepo.table
          .sortBy(_.id.desc)
          .filter(i => i.uid === uid && i.status === Approval.Status.waiting)
      approval <- table if (manifest.approvalId === approval.id)
      content <- contentRepo.table if (approval.contentId === content.id)
    } yield (approval, content)

    // total results
    val totalCount = Await.result(db.run(query.length.result), Duration.Inf)

    // pagination
    if (offset.isDefined)
      query = query.drop(offset.get)

    if (limit.isDefined)
      query = query.take(limit.get)

    query.result.map(data => new Pagination(data, totalCount, limit, offset))
  }

  def submit(authInfo: AuthInfo, contentType: Char, contentId: Long)(implicit mrh: MessagesRequestHeader) = {
    contentRepo.find(contentId).flatMap {
      case None => Future(None)
      case Some(content) =>
        contentRepo.update(contentId, content.copy(status = Content.Status.submitted), None)

        create(Approval(
          uid = authInfo.uid,
          contentType = contentType,
          contentId = contentId
        )).map { approval =>
          flowRepo.findByPosition(authInfo.orgId.getOrElse(0), authInfo.posId.getOrElse(0)).map {
            case Some(flow) =>
              val flowDetails = Await.result(flowDetailRepo.findByFlowId(flow.id.getOrElse(0)), Duration.Inf)

              if (flowDetails.length > 0) {
                val users = flowDetails.groupBy(_.orgId).flatMap { case(orgId, positions) =>
                  getUserByPosition(orgId, positions.map(_.posId)).validate[Seq[AuthInfo]] match {
                    case s: JsSuccess[Seq[AuthInfo]] => s.get
                    case e: JsError =>
                      Logger.error(e.toString)
                      Seq()
                  }
                }

                var isMandatory: Boolean = false
                val manifests = flowDetails.map { det =>
                  val uid = users.find { auth =>
                    det.posId == auth.posId.getOrElse(0) && det.orgId == auth.orgId.getOrElse(0)
                  } match {
                    case Some(auth) =>
                      // send approval notification by email
                      val detailUrl = "%s/approval/show/%d".format(config.get[String]("client.admin.endpoint"),
                          approval.id.getOrElse(0))

                      mail.send(Email(
                        s"${config.get[String]("app.name")} Approval",
                        config.get[String]("play.mailer.from"),
                        Seq(auth.email),
                        bodyHtml = Some(views.html.mail.approval(content, authInfo, detailUrl).toString)
                      ))

                      Some(auth.uid)
                    case None => None
                  }

                  var manifest = ApprovalManifest(
                    approvalId = approval.id.getOrElse(0),
                    uid = uid,
                    isMandatory = det.isMandatory,
                    orderNum = det.orderNum,
                  )

                  if (uid.isDefined && (!isMandatory || manifest.orderNum == 0))
                    manifest = manifest.copy(status = Approval.Status.waiting)

                  isMandatory = manifest.isMandatory
                  manifest
                }

                manifestRepo.createAll(manifests)
              }

            case None =>
              Logger.error("Flow Configuration Not Found.")
          }

          Some(approval)
        }
    }
  }

  def getUserByPosition(orgId: Long, posIds: Seq[Long]) = {
    val endpoint = config.get[String]("client.auth.endpoint")
    val body = Seq(("org_id", orgId.toString)) ++ posIds.map(posId => ("pos_id[]", posId.toString))
    val res = sttp.body(body : _*).post(uri"$endpoint/user/by-position").send()

    Json.parse(res.body.fold(x => x, x => x))
  }
}