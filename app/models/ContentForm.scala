package models

import play.api.data.Form
import play.api.data.Forms._
import play.api.libs.json._

case class ContentForm(
  name: String,
  body: String,
  categoryIds: Option[List[Long]] = None,
  links: Option[String] = None,
  attributes: Option[String] = None,
  submit: Option[Boolean] = Some(false),
) {
  def asModel(
    content: Option[Content] = None,
    contentType: Char = 'x',
    privilege: Char = Content.Privilege.public,
    owner: String = ""
  ) = {
    val _attributes = attributes match {
      case Some(values) => Some(Json.parse(values))
      case None => content.fold[Option[JsValue]](None)(_.attributes)
    }
    val _links = links match {
      case Some(values) => Some(values.split("\\,").toList)
      case None => content.fold[Option[List[String]]](None)(_.links)
    }

    content match {
      case Some(o) =>
        o.copy(
          name = name,
          body = body,
          attributes = _attributes,
          links = _links
        )
      case None =>
        Content(
          contentType = contentType.toString,
          owner = owner,
          name = name,
          body = body,
          privilege = privilege,
          attributes = _attributes,
          links = _links
        )
    }
  }
}

object ContentForm {
  val form = Form(
    mapping(
      "name" -> nonEmptyText,
      "body" -> nonEmptyText,
      "categoryIds" -> optional(list(longNumber)),
      "links" -> optional(text),
      "attributes" -> optional(text),
      "submit" -> optional(boolean),
    )(ContentForm.apply)(ContentForm.unapply)
  )
}