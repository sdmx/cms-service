package models

object ContentType {
  val types = Map(
    "publication" -> 'p',
    "legal" -> 'l',
    "news" -> 'n',
  )

  def getCodeByName(name: String): Char = types.getOrElse(name, 'x')

  def getNameByCode(code: Char): String = types.map(_.swap).getOrElse(code, "Unknown")

  def getNameByCode(code: String): String = getNameByCode(code.charAt(0))
}