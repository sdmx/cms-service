package models

import models.traits._
import org.joda.time.DateTime
import play.api.libs.json._

case class Approval(
  id: Option[Long] = None,
  uid: String,
  contentType: Char,
  contentId: Long,
  status: Char = Approval.Status.waiting,
  created: DateTime = DateTime.now(),
)

object Approval extends OptionJsonFormat with CharJsonFormat with DateTimeJsonFormat {
  implicit val format: OFormat[Approval] = Json.format[Approval]

  object Status {
    val hidden = '0'
    val waiting = '1'
    val approved = '2'
    val rejected = '3'
  }
}
