package models

import models.traits._
import org.joda.time.DateTime
import play.api.libs.json._

case class AuthInfo(
  uid: String,
  userType: String,
  name: String,
  email: String,
  orgId: Option[Long] = None,
  posId: Option[Long] = None,
  lastLogin: Option[DateTime] = None,
  created: Option[DateTime] = None,
)

object AuthInfo extends OptionJsonFormat with DateTimeJsonFormat {
  implicit val format: OFormat[AuthInfo] = Json.format[AuthInfo]
}
