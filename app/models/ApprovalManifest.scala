package models

import models.traits._
import org.joda.time.DateTime
import play.api.libs.json.{Json, OFormat}

case class ApprovalManifest(
  id: Option[Long] = None,
  approvalId: Long,
  orderNum: Long,
  uid: Option[String] = None,
  notes: Option[String] = None,
  isMandatory: Boolean = true,
  isEdited: Boolean = false,
  status: Char = Approval.Status.hidden,
  created: DateTime = DateTime.now(),
  updated: DateTime = DateTime.now(),
)

object ApprovalManifest extends OptionJsonFormat with DateTimeJsonFormat with CharJsonFormat {
  implicit val format: OFormat[ApprovalManifest] = Json.format[ApprovalManifest]
}
