package models

import play.api.data.Form
import play.api.data.Forms._

case class ApprovalForm(
  // contentId: Long,
  notes: Option[String],
)

object ApprovalForm {
  val form = Form(
    mapping(
      // "content_id" -> longNumber,
      "notes" -> optional(text),
    )(ApprovalForm.apply)(ApprovalForm.unapply)
  )
}
