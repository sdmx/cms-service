package models

import java.sql.Timestamp

import javax.inject._
import org.joda.time.DateTime
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

@Singleton
class ApprovalManifestRepository @Inject()(
  dbConfigProvider: DatabaseConfigProvider,
) (implicit ec: ExecutionContext) {
  protected val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  implicit def dateTimeMapper =
    MappedColumnType.base[DateTime, Timestamp](
      dt => new Timestamp(dt.getMillis),
      ts => new DateTime(ts.getTime)
    )

  class ApprovalManifestTable(tag: Tag) extends Table[ApprovalManifest](tag, "approval_manifest") {
    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)
    def approvalId = column[Long]("approval_id")
    def orderNum = column[Long]("order_num")
    def uid = column[Option[String]]("uid")
    def notes = column[Option[String]]("notes")
    def isMandatory = column[Boolean]("is_mandatory")
    def isEdited = column[Boolean]("is_edited")
    def status = column[Char]("status")
    def created = column[DateTime]("created")
    def updated = column[DateTime]("updated")
    def * = (
      id,
      approvalId,
      orderNum,
      uid,
      notes,
      isMandatory,
      isEdited,
      status,
      created,
      updated,
    ) <> ((ApprovalManifest.apply _).tupled, ApprovalManifest.unapply)
  }

  val table = TableQuery[ApprovalManifestTable]

  def create(o: ApprovalManifest): Future[ApprovalManifest] = {
    db.run(table returning table.map(_.id) into ((item, id) => item.copy(id = id)) += o)
  }

  def createAll(manifests: Seq[ApprovalManifest]): Future[Unit] = {
    db.run(DBIO.seq(
      table returning table.map(_.id) into ((item, id) => item.copy(id = id)) ++= manifests
    ))
  }

  def find(id: Long): Future[Option[ApprovalManifest]] = {
    db.run(table.filter(_.id === id).result.headOption)
  }

  def findByApprovalId(approvalId: Long): Future[Seq[ApprovalManifest]] = {
    db.run(table.filter(_.approvalId === approvalId).sortBy(_.orderNum.asc).result)
  }

  def update(id: Long, o: ApprovalManifest): Future[ApprovalManifest] = {
    db.run(table.filter(_.id === id).update(o)).map(_ => o)
  }

  def delete(id: Long): Future[Int] = db.run(table.filter(_.id === id).delete)
}