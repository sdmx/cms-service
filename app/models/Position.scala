package models

import models.traits._
import org.joda.time.DateTime
import play.api.libs.json._

case class Position(
  id: Option[Long] = None,
  parentId: Option[Long] = None,
  name: String,
  hasOrg: Boolean = true,
)

object Position extends DateTimeJsonFormat with OptionJsonFormat {
  implicit val format: OFormat[Position] = Json.format[Position]
}
