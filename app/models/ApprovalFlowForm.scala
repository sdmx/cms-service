package models

import play.api.data.Form
import play.api.data.Forms._

case class ApprovalFlowForm(
  orgId: Long,
  posId: Long,
  flowOrganisationIds: List[Long],
  flowPositionIds: List[Long],
  flowIsMandatory: List[Boolean],
)

object ApprovalFlowForm {
  val form = Form(
    mapping(
      "org_id" -> longNumber,
      "pos_id" -> longNumber,
      "flow[org_id]" -> list(longNumber),
      "flow[pos_id]" -> list(longNumber),
      "flow[is_mandatory]" -> list(boolean),
    )(ApprovalFlowForm.apply)(ApprovalFlowForm.unapply)
  )
}
