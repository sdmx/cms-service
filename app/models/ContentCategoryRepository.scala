package models


import javax.inject._
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent._

@Singleton
class ContentCategoryRepository @Inject()(
  dbConfigProvider: DatabaseConfigProvider,
  categoryRepo: CategoryRepository,
)(implicit ec: ExecutionContext) {
  protected val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  class ContentCategoryTable(tag: Tag) extends Table[ContentCategory](tag, "content_category") {
    def contentId = column[Long]("content_id")
    def categoryId = column[Long]("category_id")
    def * = (contentId, categoryId) <> ((ContentCategory.apply _).tupled, ContentCategory.unapply)
  }

  val table = TableQuery[ContentCategoryTable]

  def create(contentId: Long, categoryIds: Seq[Long]) = {
    val categories = for (category_id <- categoryIds) yield (contentId, category_id)

    db.run(table.map(row => (row.contentId, row.categoryId)) ++= categories)
  }

  def recreate(contentId: Long, categoryIds: Seq[Long]) = {
    val categories = for (category_id <- categoryIds) yield (contentId, category_id)

    db.run(DBIO.seq(
      table.filter(_.contentId === contentId).delete,
      table.map(row => (row.contentId, row.categoryId)) ++= categories
    ).transactionally)
  }

  def getCategories(contentId: Long) = {
    var query = for {
      (_, c) <- table.filter(_.contentId === contentId) joinLeft categoryRepo.table on (_.categoryId === _.id)
    } yield c

    db.run(query.result)
  }
}