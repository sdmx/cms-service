package models

import play.api.data.Form
import play.api.data.Forms._

case class ApprovalManifestForm(
  approvalId: Long,
  uid: String,
  notes: Option[String] = None,
  isMandatory: Boolean = true,
) {
//  def asModel = ApprovalManifestForm.toModel(this)
}

object ApprovalManifestForm {
  val form = Form(
    mapping(
      "approval_id" -> longNumber,
      "uid" -> nonEmptyText,
      "notes" -> optional(text),
      "is_mandatory" -> boolean,
    )(ApprovalManifestForm.apply)(ApprovalManifestForm.unapply)
  )

//  def toModel(data: ApprovalManifestForm) = ApprovalManifest(
//    approvalId = data.approvalId,
//    uid = data.uid,
//    notes = data.notes,
//    isMandatory = data.isMandatory,
//  )
}
