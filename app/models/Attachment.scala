package models

import models.traits._
import org.joda.time.DateTime
import play.api.libs.json._


case class Attachment(
  id: Option[Long] = None,
  contentId: Long,
  displayName: String = "",
  filepath: String = "",
  created: DateTime = DateTime.now(),
)

object Attachment extends DateTimeJsonFormat with OptionJsonFormat {
  implicit val format: OFormat[Attachment] = Json.format[Attachment]
}
