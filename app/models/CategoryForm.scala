package models

import play.api.data.Form
import play.api.data.Forms._

case class CategoryForm(
  name: String,
  parentId: Option[Long] = None,
)

object CategoryForm {
  val form = Form(
    mapping(
      "name" -> nonEmptyText,
      "parentId" -> optional(longNumber),
    )(CategoryForm.apply)(CategoryForm.unapply)
  )
}