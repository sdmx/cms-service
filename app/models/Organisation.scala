package models

import models.traits._
import org.joda.time.DateTime
import play.api.libs.json._

case class Organisation(
  id: Option[Long] = None,
  parentId: Option[Long] = None,
  name: String,
  orgType: Char = Organisation.OrgType.Division,
)

object Organisation extends DateTimeJsonFormat with OptionJsonFormat with CharJsonFormat {
  implicit val format: OFormat[Organisation] = Json.format[Organisation]

  object OrgType {
    val Division = '1'
    val Department = '2'
  }
}
