package models

import javax.inject._
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.duration.Duration
import scala.concurrent._

@Singleton
class ApprovalFlowDetailRepository @Inject()(
  dbConfigProvider: DatabaseConfigProvider,
) (implicit ec: ExecutionContext) {
  protected val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  class ApprovalFlowDetailTable(tag: Tag)
    extends Table[ApprovalFlowDetail](tag, "approval_flow_detail") {
    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)
    def approvalFlowId = column[Long]("approval_flow_id")
    def orgId = column[Long]("org_id")
    def posId = column[Long]("pos_id")
    def orderNum = column[Long]("order_num")
    def isMandatory = column[Boolean]("is_mandatory")

    def * = (
      id,
      approvalFlowId,
      orgId,
      posId,
      orderNum,
      isMandatory,
    ) <> ((ApprovalFlowDetail.apply _).tupled, ApprovalFlowDetail.unapply)
  }

  val table = TableQuery[ApprovalFlowDetailTable]

  def create(o: ApprovalFlowDetail): Future[ApprovalFlowDetail] = db.run(
    table returning table.map(_.id) into ((item, id) => item.copy(id = id)) += o
  )

  def attachDetails(details: Seq[ApprovalFlowDetail]) = {
    db.run(DBIO.seq(
      table returning table.map(_.id) into ((item, id) => item.copy(id = id)) ++= details
    ).transactionally)
  }

  def find(id: Long): Future[Option[ApprovalFlowDetail]] = {
    db.run(table.filter(_.id === id).result.headOption)
  }

  def findByFlowId(id: Long): Future[Seq[ApprovalFlowDetail]] = {
    db.run(table.filter(_.approvalFlowId === id).result)
  }

  def update(id: Long, o: ApprovalFlowDetail): Future[ApprovalFlowDetail] = {
    db.run(table.filter(_.id === id).update(o)).map(_ => o)
  }

  def delete(id: Long): Future[Int] = db.run(table.filter(_.id === id).delete)

  def deleteByFlowId(id: Long): Future[Int] = {
    db.run(table.filter(_.approvalFlowId === id).delete)
  }
}