package models

import models.traits._
import org.joda.time.DateTime
import play.api.libs.json._

case class ApprovalFlow(
  id: Option[Long] = None,
  uid: String,
  contentType: Char,
  orgId: Long,
  posId: Long,
  created: DateTime = DateTime.now(),
  updated: DateTime = DateTime.now(),
)

object ApprovalFlow extends OptionJsonFormat with CharJsonFormat with DateTimeJsonFormat {
  implicit val format: OFormat[ApprovalFlow] = Json.format[ApprovalFlow]
}
