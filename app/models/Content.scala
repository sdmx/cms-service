package models

import models.traits._
import org.joda.time.DateTime
import play.api.libs.json._

case class Content(
  id: Option[Long] = None,
  contentType: String,
  name: String,
  body: String,
  owner: String,
  thumbnail: Option[String] = None,
  links: Option[List[String]] = None,
  attributes: Option[JsValue] = None,
  isPublished: Boolean = false,
  status: Char = Content.Status.draft,
  privilege: Char = Content.Privilege.public,
  created: DateTime = DateTime.now(),
  updated: DateTime = DateTime.now(),
)

object Content extends DateTimeJsonFormat with OptionJsonFormat with CharJsonFormat {
  implicit val format: OFormat[Content] = Json.format[Content]

  object ContentType {
    val news = "n"
    val publication = "p"
    val regulation = "l"
    val all = Map(
      "news" -> news,
      "publication" -> publication,
      "regulation" -> regulation,
    )

    def getCodeByName(typeName: String) = all.get(typeName)
    def getNameByCode(typeCode: String) = {
      typeCode match {
        case "n" => "News"
        case "p" => "Publication"
        case "l" => "Regulation"
      }
    }
  }

  object Status {
    val draft = '0'
    val submitted = '1'
    val approved = '2'
    val rejected = '3'
    val all = Map(
      "draft" -> draft,
      "submitted" -> submitted,
      "approved" -> approved,
      "rejected" -> rejected,
    )

    def getCodeByName(statusName: String) = all.getOrElse(statusName, draft)
  }

  object Privilege {
    val public = '1'
    val internal = '2'
    val all = Map(
      "public" -> public,
      "internal" -> internal,
    )

    def getCodeByName(pvlName: String) = all.getOrElse(pvlName, public)
  }
}
