package models

import javax.inject._
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.duration.Duration
import scala.concurrent._

@Singleton
class PositionRepository @Inject()(dbConfigProvider: DatabaseConfigProvider) (implicit ec: ExecutionContext) {
  protected val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  class PositionTable(tag: Tag) extends Table[Position](tag, "position") {
    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)
    def parentId = column[Option[Long]]("parent_id")
    def name = column[String]("name")
    def hasOrg = column[Boolean]("has_organisation")
    def * = (id, parentId, name, hasOrg) <> ((Position.apply _).tupled, Position.unapply)
  }

  val table = TableQuery[PositionTable]

  def create(o: Position): Future[Position] = {
    db.run(table returning table.map(_.id) into ((item, id) => item.copy(id = id)) += o)
  }

  def find(id: Long): Future[Option[Position]] = {
    db.run(table.filter(_.id === id).result.headOption)
  }

  def update(id: Long, o: Position): Future[Position] = {
    db.run(table.filter(_.id === id).update(o)).map(_ => o)
  }

  def delete(id: Long): Future[Int] = {
    db.run(table.filter(_.id === id).delete)
  }

  def list(
    q: Option[String] = None,
    limit: Option[Long] = None,
    offset: Option[Long] = None,
    sort: Option[Seq[(String, String)]] = None
  ): Future[Pagination[Position]] = db.run {
    var query = table.sortBy(_.id.desc)

    // filter by key
    if (q.isDefined)
      query = table.filter(_.name.like(s"%${q.get}%"))

    // total results
    val totalCount = Await.result(db.run(query.length.result), Duration.Inf)

    // pagination
    if (offset.isDefined)
      query = query.drop(offset.get)

    if (limit.isDefined)
      query = query.take(limit.get)

    query.result.map(data => new Pagination(data, totalCount, limit, offset))
  }
}