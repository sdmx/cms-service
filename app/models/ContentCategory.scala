package models

import play.api.libs.json._

case class ContentCategory(
  contentId: Long,
  categoryId: Long
)

object ContentCategory {
  implicit val format: OFormat[Category] = Json.format[Category]
}
