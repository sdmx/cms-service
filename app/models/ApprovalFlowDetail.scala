package models

import models.traits._
import play.api.libs.json.{Json, OFormat}

case class ApprovalFlowDetail(
  id: Option[Long] = None,
  approvalFlowId: Long,
  orgId: Long,
  posId: Long,
  orderNum: Long,
  isMandatory: Boolean = true,
)

object ApprovalFlowDetail extends OptionJsonFormat {
  implicit val format: OFormat[ApprovalFlowDetail] = Json.format[ApprovalFlowDetail]
}