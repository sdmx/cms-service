package models

import java.sql.Timestamp

import javax.inject._
import org.joda.time.DateTime
import play.api.db.slick.DatabaseConfigProvider
import play.api.libs.Files.TemporaryFile
import play.api.mvc.MultipartFormData.FilePart
import slick.jdbc.JdbcProfile
import utils.IO

import scala.concurrent._
import scala.concurrent.duration.Duration

@Singleton
class AttachmentRepository @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {
  val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  class AttachmentTable(tag: Tag) extends Table[Attachment](tag, "attachment") {
    implicit def dateTimeMapper = MappedColumnType.base[DateTime, Timestamp](
      dt => new Timestamp(dt.getMillis),
      ts => new DateTime(ts.getTime)
    )

    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)
    def contentId = column[Long]("content_id")
    def displayName = column[String]("display_name")
    def filepath = column[String]("filepath")
    def created = column[DateTime]("created")

    def * = (
      id,
      contentId,
      displayName,
      filepath,
      created,
    ) <> ((Attachment.apply _).tupled, Attachment.unapply)
  }

  val table = TableQuery[AttachmentTable]

  def find(id: Long): Future[Option[Attachment]] = {
    db.run(table.filter(_.id === id).result.headOption);
  }

  def attachFiles(id: Long, attachments: Seq[Attachment]): Future[Unit] = {
    db.run(DBIO.seq(
      table returning table.map(_.id)
        into ((item, id) => item.copy(id = id)) ++= attachments
    ).transactionally)
  }

  def attachFiles(content: Content, files: Seq[FilePart[TemporaryFile]]): Future[Unit] = {
    if (content.id.isEmpty)
      Future.unit

    val attachments = for (file <- files) yield {
      val filename = java.util.UUID.randomUUID.toString +"."+ IO.extension(file.filename)
      val fileuri = content.contentType +"/"+ filename
      val filepath = IO.storage/fileuri

      filepath.createIfNotExists(false, true)
      file.ref.moveTo(filepath.toJava, true)

      Attachment(None, content.id.get, file.filename, fileuri)
    }

    attachFiles(content.id.get, attachments)
  }

  def attachFiles(contentType: String, id: Long, files: Seq[FilePart[TemporaryFile]]): Future[Unit] = {
    val attachments = for (file <- files) yield {
      val filename = java.util.UUID.randomUUID.toString +"."+ IO.extension(file.filename)
      val fileuri = contentType +"/"+ filename
      val filepath = IO.storage/fileuri

      filepath.createIfNotExists(false, true)
      file.ref.moveTo(filepath.toJava, true)

      Attachment(None, id, file.filename, fileuri)
    }

    attachFiles(id, attachments)
  }

  def getByContent(id: Long): Future[Seq[Attachment]] = {
    db.run(table.filter(_.contentId === id).result);
  }

  def delete(id: Long): Future[Int] = find(id).map(attachment => {
    if (attachment.isDefined) {
      IO.delete(attachment.get.filepath)
      db.run(table.filter(_.id === id).delete)

      1
    }
    else 0
  })

  def deleteByContent(id: Long): Future[Int] = {
    getByContent(id).map(files => {
      // remove physical files
      files.map(attachment => IO.delete(attachment.filepath))

      // remove data from database
      Await.result(db.run(table.filter(_.contentId === id).delete), Duration.Inf)
    })
  }
}
