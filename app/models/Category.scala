package models

import models.traits._
import play.api.libs.json._

case class Category(
  id: Option[Long] = None,
  name: String,
  parentId: Option[Long] = None,
)

object Category extends OptionJsonFormat with CharJsonFormat {
  implicit val format: OFormat[Category] = Json.format[Category]
}
