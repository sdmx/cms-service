package models

import java.sql.Timestamp

import javax.inject._
import org.joda.time.DateTime
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

@Singleton
class ApprovalFlowRepository @Inject()(
  dbConfigProvider: DatabaseConfigProvider,
)(implicit ec: ExecutionContext) {
  protected val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  implicit def dateTimeMapper =
    MappedColumnType.base[DateTime, Timestamp](
      dt => new Timestamp(dt.getMillis),
      ts => new DateTime(ts.getTime)
    )

  class ApprovalFlowTable(tag: Tag) extends Table[ApprovalFlow](tag, "approval_flow") {
    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)
    def uid = column[String]("uid")
    def contentType = column[Char]("content_type")
    def orgId = column[Long]("org_id")
    def posId = column[Long]("pos_id")
    def created = column[DateTime]("created")
    def updated = column[DateTime]("updated")
    def * = (
      id,
      uid,
      contentType,
      orgId,
      posId,
      created,
      updated,
    ) <> ((ApprovalFlow.apply _).tupled, ApprovalFlow.unapply)
  }

  val table = TableQuery[ApprovalFlowTable]

  def create(flow: ApprovalFlow): Future[ApprovalFlow] = {
    db.run(table returning table.map(_.id) into ((item, id) => item.copy(id = id)) += flow)
  }

  def findOrCreate(flow: ApprovalFlow): Future[ApprovalFlow] = {
    val flowRes = db.run(table.filter(row =>
      row.uid === flow.uid
      && row.contentType === flow.contentType
      && row.orgId === flow.orgId
      && row.posId === flow.posId
    ).take(1).result.headOption)

    flowRes.flatMap {
      case None => create(flow)
      case Some(newFlow) => Future(newFlow)
    }
  }

  def findByType(contentType: Char, uid: String): Future[Option[ApprovalFlow]] = db.run(
    table.filter(i => i.contentType === contentType && i.uid === uid).take(1).result.headOption
  )

  def find(id: Long): Future[Option[ApprovalFlow]] = {
    db.run(table.filter(_.id === id).take(1).result.headOption)
  }

  def findByPosition(orgId: Long, posId: Long): Future[Option[ApprovalFlow]] = {
    db.run(table.filter(x => x.orgId === orgId && x.posId === posId).result.headOption)
  }

  def findByUserPosition(uid: String, posId: Long): Future[Seq[ApprovalFlow]] = {
    db.run(table.filter(x => x.uid === uid && x.posId === posId).result)
  }

  def update(id: Long, flow: ApprovalFlow): Future[ApprovalFlow] = {
    db.run(table.filter(_.id === id).update(flow)).map(_ => flow)
  }

  def delete(id: Long): Future[Int] = db.run(table.filter(_.id === id).delete)

  def list(
    uid: String,
    limit: Option[Long] = None,
    offset: Option[Long] = None,
    sort: Option[Seq[(String, String)]] = None
  ): Future[Pagination[ApprovalFlow]] = db.run {
    var query = table.sortBy(_.id.desc).filter(_.uid === uid)

    // total results
    val totalCount = Await.result(db.run(query.length.result), Duration.Inf)

    // pagination
    if (offset.isDefined)
      query = query.drop(offset.get)

    if (limit.isDefined)
      query = query.take(limit.get)

    query.result.map(data => new Pagination(data, totalCount, limit, offset))
  }
}