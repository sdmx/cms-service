package errors

import javax.inject.Singleton
import play.api.Logger
import play.api.http.HttpErrorHandler
import play.api.mvc._
import play.api.mvc.Results._

import scala.concurrent.Future

@Singleton
class ErrorHandler extends HttpErrorHandler {

  override def onClientError(request: RequestHeader, statusCode: Int, message: String) = {
    Future.successful(Status(statusCode)("A client error occurred: " + message))
  }

  override def onServerError(request: RequestHeader, exception: Throwable) = {
    Logger.error(exception.getMessage, exception)
    Future.successful(InternalServerError("A server error occurred: " + exception.getMessage))
  }
}
